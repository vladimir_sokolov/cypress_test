describe('Awescode Test Input form', () => {
  it('Send Positive Data to Test Form', () => {

  cy
    .visit('http://company.test-awescrm.de/test/test-simple-form')
    
  cy
    .get(':nth-child(1) > .input > .v-popover > .trigger > .input__field')
    .should('be.visible').and('be.enabled')
    .type('titleMoreThanEightSymbols')
   
  cy
    .get(':nth-child(2) > .input > .v-popover > .trigger > .input__field')
      .should('be.visible').and('be.enabled')
    .type('description')
      
  cy.get('.btn')
    .should('be.enabled')
    .click()
  
  cy
    .get('.vue-notification')
    .should('be.visible')

  cy
    .get('.vue-notification__title')
    .contains('success')
    .should('exist')

  })
})