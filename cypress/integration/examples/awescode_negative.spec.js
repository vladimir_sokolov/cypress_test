describe('Awescode Test Input form', () => {
  it('Send Negative Data to Test Form', () => {

  cy
    .visit('http://company.test-awescrm.de/test/test-simple-form')
    
  cy
    .get(':nth-child(1) > .input > .v-popover > .trigger > .input__field')
    .should('be.visible').and('be.enabled')
    .type('titleMoreThanEightSymbols')
   
  cy
    .get(':nth-child(2) > .input > .v-popover > .trigger > .input__field')
      .should('be.visible').and('be.enabled')
    .type(' ')
      
  cy.get('.btn')
    .should('be.enabled')
    .click()
  
  cy
    .get('.tooltip__text')
    .contains('The title must be at least 8 characters')
    .should('exist')
    
    cy
      .get('.tooltip__text')
      .contains('The description field is required')
      .should('exist')

  })
})